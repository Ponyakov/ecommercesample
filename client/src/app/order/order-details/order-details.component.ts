import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IOrder, IOrderItem } from 'src/app/shared/models/order';
import { BreadcrumbService } from 'xng-breadcrumb';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
  order: IOrder;
  orderItems: IOrderItem[];
  shippingPrice: number;
  subtotal: number;
  total: number;

  constructor(private bcService: BreadcrumbService,
              private activatedRoute: ActivatedRoute,
              private orderService: OrderService) {
    bcService.set('@orderDetails', '  ');
  }

  ngOnInit(): void {
    this.loadOrder();
  }

  loadOrder(): void {
    this.orderService.getOrder(+this.activatedRoute.snapshot.paramMap.get('id'))
      .subscribe(order => {
        this.order = order;
        this.calculateTotals();
        this.bcService.set('@orderDetails', 'Order# ' + order.id + ' - ' + order.status);
        this.orderItems = order.orderItems;
      });
  }

  private calculateTotals(): void {
    this.shippingPrice = this.order.shippingPrice;
    this.subtotal = this.order.orderItems.reduce((a, b) => (b.price * b.quantity) + a, 0);
    this.total = this.subtotal + this.shippingPrice;
  }

}
